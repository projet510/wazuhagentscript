Ce fichier bash permet l'automatisation de l'installation du Wazuh Agent sous Linux (grâce à Ansible par exemple).



    ****************************************    Prérequis:   ********************************************************* 

 
- Avoir les privilèges administrateur sur le système (sudo ou root).
- Avoir un accès internet.
- S'assurer que vous possedez la commande "curl" : "curl --version"
Si "curl" est introuvable veuillez l'installer en saisissant la commande suivante : "sudo apt-get install curl"

 Vous pouvez maintenant executer ce programme!



    ************************************    Execution du programme:    ********************************************


    1. Installation de Wazuh Agent(ligne 3):

    Ce programme va commencer par chercher le package de Wazuh Agent sur le site de Wazuh et l'installer grâce à la commande "curl" (ligne 3).


    2. Liaison au Wazuh Manager(ligne 9): 

    Il va ensuite lié ce Wazuh Agent à un wazuh manager (PC1 de la salle Noether dans ce cas).
    Cette ligne est directement générée par votre Wazuh Manager (Wazuh Manager API -> ajouter un Agent) et sera donc à   remplacer par votre propre commande.(ligne 9)
    Dans cette commande l'IP est celle de votre server Manager.


    3. Modification du nom dans le fichier de configuration(ligne 12/25):

    Lorsque vous automatisez l'installation de Wazuh agent sur plusieurs PC, (notamment grâce à Ansible) vous allez avoir besoin d'éditer le nom des  différents PC.
    Wazuh Manager n'accepte que des noms d'Agent unique. Donc chaque PC doit avoir un nom unique dans son fichier de configuration pour être accepté par votre manager.

    Nous allons donc aller chercher l'adresse IP de notre PC et l'utiliser comme nom unique dans notre fichier "ossec.conf".
    Les différents Agent seront donc identifiés dans le Manager par leur IP.

    Pour cela nous allons écrire puis executer un fichier python qui va placer l'IP de notre PC au bon endroit dans le fichier de configuration.


    4. Activation et lancement du service(ligne 32/34):

    On "enable" puis lance le service "Wazuh manager" qui devrait alors apparaitre dans votre Manager.




*************************************************    Liens utiles:    ****************************************************


Site web de Wazuh : https://wazuh.com/

Documentation de Wazuh : https://documentation.wazuh.com/


