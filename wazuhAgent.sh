#!/bin/bash

sudo curl -s https://packages.wazuh.com/key/GPG-KEY-WAZUH | gpg --no-default-keyring --keyring gnupg-ring:/usr/share/keyrings/wazuh.gpg --import && chmod 644 /usr/share/keyrings/wazuh.gpg

echo "deb [signed-by=/usr/share/keyrings/wazuh.gpg] https://packages.wazuh.com/4.x/apt/ stable main" | tee -a /etc/apt/sources.list.d/wazuh.list

sudo apt-get update

sudo curl -so wazuh-agent-4.3.10.deb https://packages.wazuh.com/4.x/apt/pool/main/w/wazuh-agent/wazuh-agent_4.3.10-1_amd64.deb && sudo WAZUH_MANAGER='10.78.120.31' WAZUH_AGENT_GROUP='default' dpkg -i ./wazuh-agent-4.3.10.deb


ifconfig eno1 | grep 'inet ' | cut -c14-25 > ip.txt
sudo chmod 755 /var/ossec
sudo chmod 755 /var/ossec/etc
sudo chmod 777 /var/ossec/etc/ossec.conf
echo "ip=open('ip.txt','r')" >changeWazuhAgentConf.py
echo "ip2=ip.readline()" >> changeWazuhAgentConf.py
echo "ip2=ip2.strip()" >> changeWazuhAgentConf.py
echo "ip.close()" >>changeWazuhAgentConf.py
echo "print(ip2)">>changeWazuhAgentConf.py
echo "f=open('/var/ossec/etc/ossec.conf','r').read().replace('<groups>default</groups>','<groups>default</groups><agent_name>{}</agent_name>'.format(ip2))" >> changeWazuhAgentConf.py
echo "f1 = open('/var/ossec/etc/ossec.conf','w')" >> changeWazuhAgentConf.py
echo "f1.write(f)" >> changeWazuhAgentConf.py
echo "f.close()"
echo "f1.close()"

python3 changeWazuhAgentConf.py

sudo rm ip.txt
sudo rm changeWazuhAgentConf.py

sudo systemctl daemon-reload
sudo systemctl enable wazuh-agent
sudo systemctl start wazuh-agent
